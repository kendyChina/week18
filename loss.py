import torch
import torch.nn as nn


class ScoreLoss(nn.Module):
	def __init__(self):
		super(ScoreLoss, self).__init__()

	def forward(self, score_pre, score_gt):
		eps = 1e-3
		# beta是 负样本个数 / 总样本数
		beta = 1. - torch.sum(score_gt) / score_gt.view(-1).size(0)
		balanced_ce = - beta * score_gt * torch.log(score_pre + eps) - \
			   (1 - beta) * (1 - score_gt) * torch.log(1 - score_pre + eps)
# 		print(balanced_ce)
# 		mask = balanced_ce == float("inf")
# 		balanced_ce.data.masked_fill(mask, 0)
# 		balanced_ce[balanced_ce == float("inf")] = 0
# 		print(balanced_ce)
# 		print(balanced_ce.sum())
# 		return torch.sum(balanced_ce)
		return balanced_ce


class RBOXLoss(nn.Module):
	def __init__(self, weight_angle):
		super(RBOXLoss, self).__init__()
		self.weight_angle = weight_angle

	def forward(self, geo_pre, geo_gt):
		# geo_pre, geo_gt: [AABB, angle]
		# AABB: [batch_size, (d1, d2, d3, d4), w, h]
		batch_size = geo_pre[0].shape[0]
		w, h = geo_pre[0].shape[2:]
		AABB_pre, angle_pre = geo_pre
		AABB_gt, angle_gt = geo_gt[:, :4, :, :], geo_gt[:, 4, :, :]

		# 把spatial维度view成1维
		AABB_pre = AABB_pre.view(batch_size, 4, -1)
		AABB_gt = AABB_gt.view(batch_size, 4, -1)
		min_d1 = torch.min(AABB_pre[:, 0, :], AABB_gt[:, 0, :])
		min_d2 = torch.min(AABB_pre[:, 1, :], AABB_gt[:, 1, :])
		min_d3 = torch.min(AABB_pre[:, 2, :], AABB_gt[:, 2, :])
		min_d4 = torch.min(AABB_pre[:, 3, :], AABB_gt[:, 3, :])

		min_w = min_d2 + min_d4
		min_h = min_d1 + min_d3

		intersection = min_w * min_h

		area_pre = (AABB_pre[:, 0, :] + AABB_pre[:, 2, :]) * \
				   (AABB_pre[:, 1, :] + AABB_pre[:, 3, :])

		area_gt = (AABB_gt[:, 0, :] + AABB_gt[:, 2, :]) * \
				  (AABB_gt[:, 1, :] + AABB_gt[:, 3, :])

		union = area_pre + area_gt - intersection

# 		loss_AABB = torch.sum(-torch.log(intersection / union))
# 		loss_angle = torch.sum(1 - torch.cos(angle_pre - angle_gt))
		loss_AABB = -torch.log(intersection + 1.0 / union + 1.0)
		loss_angle = 1 - torch.cos(angle_pre - angle_gt)
		loss_AABB = loss_AABB.view(batch_size, 1, w, h)
		loss_geo = loss_AABB + self.weight_angle * loss_angle
		loss_geo[loss_geo == float("inf")] = 0
		return loss_geo


class Loss(nn.Module):
	def __init__(self, weight_geo, weight_angle, geometry_mode="RBOX"):
		super(Loss, self).__init__()

		self.weight_geo = weight_geo
		self.weight_angle = weight_angle
		self.score_loss = ScoreLoss()
		self.geometry_mode = geometry_mode

		if self.geometry_mode == "RBOX":
			self.geo_loss = RBOXLoss(self.weight_angle)
		else:
			self.geo_loss = RBOXLoss(self.weight_angle)

	def forward(self, score_pre, geo_pre, score_gt, geo_gt):
		score_loss = self.score_loss(score_pre, score_gt)
		geo_loss = self.geo_loss(geo_pre, geo_gt)
		print(score_loss)
		print(geo_loss)
		total_score = score_loss + self.weight_geo * geo_loss
		print(torch.sum(total_score))
		return score_loss + self.weight_geo * geo_loss
