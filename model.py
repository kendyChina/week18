import math

import torch
import torch.nn as nn
import torch.nn.functional as F

from models.VGG16 import VGG16


class EAST(nn.Module):
	def __init__(self, extractor="PVANet", geometry_mode="RBOX", pretrained=True, **kwargs):
		super(EAST, self).__init__()

		extractor_channels = [16, 64, 128, 256, 384]
		if extractor == "PVANet":
			self.extractor = PVANet(extractor_channels, **kwargs)
		else:
			self.extractor = VGG16(pretrained, **kwargs)


		merge_channels = [128, 64, 32]
		self.merging_branch = MergingBranch(extractor_channels, merge_channels, **kwargs)

		self.output_layer = OutputLayer(merge_channels[-1], geometry_mode)

	def forward(self, x):
		bridges = self.extractor(x)
		feature = self.merging_branch(bridges)
		return self.output_layer(feature)


def conv_stage(in_channels, out_channels, **kwargs):
	stage = [
		nn.Conv2d(in_channels, out_channels, 3, stride=2, padding=1, bias=False, **kwargs),
		nn.BatchNorm2d(out_channels),
		nn.ReLU(inplace=True)
	]
	return nn.Sequential(*stage)


class PVANet(nn.Module):
	def __init__(self, channels, **kwargs):
		super(PVANet, self).__init__()

		self.conv1 = nn.Conv2d(3, channels[0], 7, stride=2, padding=3, bias=False)
		conv_stages = []
		for i in range(len(channels) - 1):
			conv_stages.append(conv_stage(channels[i], channels[i + 1], **kwargs))
		self.conv_stages = conv_stages

		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
				if m.bias is not None:
					nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.BatchNorm2d):
				nn.init.constant_(m.weight, 1)
				nn.init.constant_(m.bias, 0)

	def forward(self, x):
		x = self.conv1(x)

		bridges = []
		for cs in self.conv_stages:
			x = cs(x)
			bridges.append(x)
		return bridges


def merge_block(in_channels, out_channels, **kwargs):
	block = [
		nn.Conv2d(in_channels, out_channels, 1, bias=False),
		nn.BatchNorm2d(out_channels),
		nn.ReLU(inplace=True),
		nn.Conv2d(out_channels, out_channels, 3, padding=1, bias=False),
		nn.BatchNorm2d(out_channels),
		nn.ReLU(inplace=True),
	]
	return nn.Sequential(*block)


class MergingBranch(nn.Module):
	def __init__(self, extractor_channels, merge_channels, **kwargs):
		super(MergingBranch, self).__init__()

		self.unpool = nn.UpsamplingBilinear2d(scale_factor=2)
		self.merge = []
		for i in range(len(merge_channels)):
			if i == 0 :
				self.merge.append(
					merge_block(extractor_channels[-i - 1] + extractor_channels[-i - 2],
								merge_channels[i])
				)
			else:
				self.merge.append(
					merge_block(merge_channels[i - 1] + extractor_channels[-i - 2],
								merge_channels[i])
				)
		self.conv = nn.Conv2d(merge_channels[-1], merge_channels[-1], 3, padding=1, bias=False)

		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
				if m.bias is not None:
					nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.BatchNorm2d):
				nn.init.constant_(m.weight, 1)
				nn.init.constant_(m.bias, 0)

	def forward(self, bridges):
		x = bridges[-1]
		for i in range(len(bridges) - 1):
			x = torch.cat((self.unpool(x), bridges[-i - 2]), 1)
			x = self.merge[i](x)
		x = self.conv(x)
		return x


class OutputLayer(nn.Module):
	def __init__(self, in_channels, geometry_mode="RBOX"):
		super(OutputLayer, self).__init__()
		self.geometry_mode = geometry_mode
		self.scope = 512

		self.score_conv = nn.Conv2d(in_channels, 1, 1)

		if self.geometry_mode == "RBOX":
			self.geo_conv1 = nn.Conv2d(in_channels, 4, 1, bias=False)
			self.geo_conv2 = nn.Conv2d(in_channels, 1, 1, bias=False)
		else:
			self.geo_conv = nn.Conv2d(in_channels, 8, 1, bias=False)

		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
				if m.bias is not None:
					nn.init.constant_(m.bias, 0)
			elif isinstance(m, nn.BatchNorm2d):
				nn.init.constant_(m.weight, 1)
				nn.init.constant_(m.bias, 0)

	def forward(self, x):
		score = torch.sigmoid(self.score_conv(x))

		if self.geometry_mode == "RBOX":
			geometry = [
				torch.sigmoid(self.geo_conv1(x)) * self.scope,  # sigmoid
				torch.sigmoid(self.geo_conv2(x) - 0.5) * math.pi  # sigmoid
			]
		else:
			geometry = self.geo_conv(x)  # sigmoid

		return score, geometry
