import os
import time
import argparse

import torch
import torch.utils.data as data
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau, MultiStepLR

from dataset import custom_dataset
from model import EAST
from loss import Loss

parser = argparse.ArgumentParser(description="EAST training")
parser.add_argument("--training_dataset", default="./pths")
parser.add_argument("-b", "--batch_size", default=1, type=int)
parser.add_argument("--gpu", default=2, type=int)
parser.add_argument('--num_workers', default=1, type=int, help='Number of workers used in dataloading')
parser.add_argument("-max", "--max_epoch", default=600, type=int)
parser.add_argument("--optimizer", default="ADAM", type=str)
parser.add_argument("--lr", "--learning_rate", default=1e-3, type=float, help="initial learning rate")
parser.add_argument("--momentum", default=0.9, type=float, help="momentum for SGD")
parser.add_argument("--weight_decay_adam", default=0., type=float, help="weight_decay for ADAM")
parser.add_argument("--weight_decay_sgd", default=5e-4, type=float, help="weight_decay for SGD")

parser.add_argument("--weight_geo", default=1.0, type=float, help="weight of Loss_geometry")
parser.add_argument("--weight_angle", default=10.0, type=float, help="weight of Loss_angle")
parser.add_argument("--extractor", default="PVANet", help="extractor of model")
parser.add_argument("--geometry_mode", default="RBOX", help="mode of output, RBOX or QUAD")
parser.add_argument("--save_folder", default="./weights/")
args = parser.parse_args()

if not os.path.exists(args.save_folder):
    os.mkdir(args.save_folder)

training_dataset = args.training_dataset
batch_size = args.batch_size
num_gpu = args.gpu
num_workers = args.num_workers
initial_lr = args.lr
momentum = args.momentum
weight_decay_sgd = args.weight_decay_sgd
weight_decay_adam = args.weight_decay_adam
max_epoch = args.max_epoch
opti = args.optimizer
weight_geo = args.weight_geo
weight_angle = args.weight_angle
extractor = args.extractor
geometry_mode = args.geometry_mode
save_folder = args.save_folder


def train(train_img_path, train_label_path):
    file_num = len(os.listdir(train_img_path))
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    net = EAST(extractor=extractor, geometry_mode=geometry_mode, pretrained=False)
#     nei = net.to(device)
    trainset = custom_dataset(train_img_path, train_label_path)
    train_loader = data.DataLoader(trainset, batch_size=batch_size,
                                   shuffle=True, num_workers=num_workers, drop_last=True)
    

    
    

    if opti.lower() == "adam":
        optimizer = optim.Adam(net.parameters(), lr=initial_lr, weight_decay=weight_decay_adam)
    else:
        optimizer = optim.SGD(net.parameters(), lr=initial_lr, momentum=momentum, weight_decay=weight_decay_sgd)
    criterion = Loss(weight_geo, weight_angle, geometry_mode="RBOX")
    scheduler = MultiStepLR(optimizer, milestones=[max_epoch // 2], gamma=0.1)

    net.train()
    epoch_loss = 0.

    for epoch in range(max_epoch):
        epoch_time = time.time()
        for i, (img, score_gt, geo_gt, ignored_map) in enumerate(train_loader):
            start_time = time.time()
#             img, score_gt, geo_gt, ignored_map = img.to(device), score_gt.to(device),\
#                                                  geo_gt.to(device), ignored_map.to(device)

            score_pred, geo_pred = net(img)
            loss = criterion(score_pred, geo_pred, score_gt, geo_gt)

            epoch_loss += loss.item()

            optimizer.zero_grad()
            loss.backward()

            optimizer.step()
            scheduler.step()
            return
            print('Epoch is [{}/{}], mini-batch is [{}/{}], time consumption is {:.8f}, batch_loss is {:.8f}'.
                  format(epoch + 1, max_epoch, i + 1, int(file_num / batch_size),
                         time.time() - start_time, loss.item()))


def adjust_learning_rate(optimizer):
    ReduceLROnPlateau(optimizer, mode="min", factor=0.5, patience=2, verbose=False,
                      threshold_mode="rel", threshold=1e-5, cooldown=0,
                      min_lr=0, eps=1e-9)


if __name__ == "__main__":
    train_img_path = os.path.abspath("./ICDAR_2015/train_img")
    train_gt_path = os.path.abspath("./ICDAR_2015/train_gt")
    train(train_img_path, train_gt_path)
